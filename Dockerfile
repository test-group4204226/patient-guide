# Use a base image with JDK and Maven installed for the build stage
FROM maven:3.9.8-eclipse-temurin-17 AS build

# Set the working directory inside the container
WORKDIR /app

# Copy the pom.xml file to install dependencies
COPY pom.xml .

# Install dependencies based on pom.xml
RUN mvn dependency:go-offline

# Copy the rest of the application code
COPY src ./src

# Build the application
RUN mvn package -DskipTests || cat /app/target/surefire-reports/*.txt

# Second stage: Use a smaller base image for the runtime environment
FROM eclipse-temurin:17-jre

# Set the working directory inside the container
WORKDIR /app

# Copy the packaged JAR file from the build stage to the runtime image
COPY --from=build /app/target/patientguide-0.0.1-SNAPSHOT.jar ./app.jar

# Make port 8081 available to the world outside this container
EXPOSE 8081

# Command to run the application
CMD ["java", "-jar", "./app.jar"]
