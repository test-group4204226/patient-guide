pipeline {
    agent any
    environment {
        DOCKER_HOST = 'unix:///var/run/docker.sock'
    }
    tools {
        nodejs 'NodeJS 22.3.0'
        maven 'Maven 3.9.8'
        dockerTool 'Docker'
    }
    parameters {
        booleanParam(name: 'BUILD_BACKEND', defaultValue: true, description: 'Build the backend')
        booleanParam(name: 'TEST_BACKEND', defaultValue: true, description: 'Test the backend')
        booleanParam(name: 'BUILD_FRONTEND', defaultValue: true, description: 'Build the frontend')
        booleanParam(name: 'TEST_FRONTEND', defaultValue: true, description: 'Test the frontend')
        booleanParam(name: 'CODE_SCAN', defaultValue: true, description: 'Code Scan the build')
        booleanParam(name: 'FRONTEND_DOCKER_IMAGE', defaultValue: true, description: 'Build Docker image for the frontend')
        booleanParam(name: 'BACKEND_DOCKER_IMAGE', defaultValue: false, description: 'Build Docker image for the backend')
        booleanParam(name: 'DEPLOY', defaultValue: false, description: 'Deploy the Docker images on AWS EC2 instances')
        string(name: 'TAG_NAME', defaultValue: 'v1.0', description: 'Git tag to build')
    }
    
    stages {
        // stage('Checkout') {
        //     steps {
        //         script {
        //             if (params.TAG_NAME) {
        //                 // Checkout the specific tag
        //                 checkout([$class: 'GitSCM', branches: [[name: "refs/tags/${params.TAG_NAME}"]], userRemoteConfigs: [[url: 'https://gitlab.com/test-group4204226/patient-guide.git']]])
        //             } else {
        //                 // Checkout the latest commit
        //                 checkout scm
        //             }
        //         }
        //     }
        // }
        stage('Clean Workspace') {
            steps {
                dir('frontend') {
                    sh 'rm -rf node_modules package-lock.json'
                }
            }
        }
        stage('Build Backend') {
            when {
                expression { return params.BUILD_BACKEND }
            }
            steps {
                sh 'mvn clean install'
            }
        }
        stage('Code Scan'){
            when{
                expression { return params.CODE_SCAN }
            }
            steps{
                echo "Code Scan"
            }
        }
        stage('Test Backend') {
            when {
                expression { return params.TEST_BACKEND }
            }
            steps {
                echo 'Running Unit Tests'
                sh 'mvn test'
            }
        }
        stage('Build Frontend') {
            when {
                expression { return params.BUILD_FRONTEND }
            }
            steps {
                dir('frontend') {
                    sh 'npm install'
                }
            }
        }
        stage('Test Frontend - Integration tests') {
            when {
                expression { return params.TEST_FRONTEND }
            }
            steps {
                dir('frontend') {
                    sh 'npx cypress run'
                }
            }
        }
        stage('Build Frontend Docker Image') {
            when {
                expression { return params.FRONTEND_DOCKER_IMAGE }
            }
            steps {
                sh "docker build -t frontend-image frontend"
            }
        }
        stage('Build Backend Docker Image') {
            when {
                expression { return params.BACKEND_DOCKER_IMAGE }
            }
            steps {
                sh "docker build -t backend-image ."
            }
        }
        stage('Deploy') {
            when {
                expression { return params.DEPLOY }
            }
            steps {
                echo 'Deploying frontend and backend docker containers on AWS EC2 instance'
            }
        }
    }

    post{        
        failure {
            emailext to: 'manasgandhi01@gmail.com', subject: 'Pipeline Failed', body: 'The Jenkins pipeline has failed.'
        }
    }
}
