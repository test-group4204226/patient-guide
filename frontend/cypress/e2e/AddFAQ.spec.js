describe('AddFAQ Component', () => {
    beforeEach(() => {
      cy.visit('/'); // Replace with the actual path for the AddFAQ component
    });
  
    it('should load the AddFAQ form', () => {
      cy.contains('h2', 'Add FAQ').should('be.visible');
      cy.get('input.add-faq-input').should('have.length', 2);
      cy.get('button.add-faq-button').should('be.enabled');
    });
  
    it('should allow a user to fill out and submit the form', () => {
      cy.intercept('POST', 'http://localhost:8081/api/faqs', {
        statusCode: 200,
      }).as('addFAQ');
  
      cy.get('input.add-faq-input').first().type('What is Cypress?');
      cy.get('input.add-faq-input').last().type('Cypress is a front-end testing tool for web applications.');
      
      cy.get('button.add-faq-button').click();
  
      cy.wait('@addFAQ').its('response.statusCode').should('eq', 200);
    });
  
    // it('should handle empty form submission', () => {
    //   cy.get('button.add-faq-button').click();
    //   cy.contains('Please fill in this field.').should('be.visible');
    //   cy.contains('Answer is required').should('be.visible');
    // });
  
    it('should handle server error', () => {
      cy.intercept('POST', 'http://localhost:8081/api/faqs', {
        statusCode: 500,
        body: { message: 'Internal Server Error' },
      }).as('addFAQ');
  
      cy.get('input.add-faq-input').first().type('Test Question');
      cy.get('input.add-faq-input').last().type('Test Answer');
      
      cy.get('button.add-faq-button').click();
  
      cy.wait('@addFAQ').its('response.statusCode').should('eq', 500);
    });
  });
  