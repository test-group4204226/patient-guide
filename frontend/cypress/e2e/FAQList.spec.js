describe('FAQList Component', () => {
    beforeEach(() => {
      cy.intercept('GET', 'http://localhost:8081/api/faqs', {
        statusCode: 200,
        body: [
          {question: 'What is Cypress?', answer: 'Cypress is a front-end testing tool for web applications.' },
          {question: 'What is React?', answer: 'React is a JavaScript library for building user interfaces.' }
        ],
      }).as('getFAQs');
  
      cy.visit('/faq-list'); // Replace with the actual path for the FAQList component
    });
  
    it('should load the FAQ list', () => {
      cy.contains('h2', 'FAQs').should('be.visible');
    });
  
    it('should display a list of FAQs', () => {
      cy.wait('@getFAQs');
  
      cy.get('.faq-list').should('exist');
      cy.get('.faq-item').should('have.length', 2);
      cy.contains('What is Cypress?').should('be.visible');
      cy.contains('What is React?').should('be.visible');
    });
  
    it('should handle an empty FAQ list', () => {
      cy.intercept('GET', 'http://localhost:8081/api/faqs', {
        statusCode: 200,
        body: [],
      }).as('getEmptyFAQs');
  
      cy.visit('/faq-list');
      cy.wait('@getEmptyFAQs');
  
      cy.get('.faq-list').should('exist');
      cy.get('.faq-item').should('have.length', 0);
    });
  
    it('should handle a large number of FAQs', () => {
      const faqs = Array.from({ length: 100 }, (_, i) => ({
        id: i + 1,
        question: `Question ${i + 1}`,
        answer: `Answer ${i + 1}`,
      }));
  
      cy.intercept('GET', 'http://localhost:8081/api/faqs', {
        statusCode: 200,
        body: faqs,
      }).as('getManyFAQs');
  
      cy.visit('/faq-list');
      cy.wait('@getManyFAQs');
  
      cy.get('.faq-list').should('exist');
      cy.get('.faq-item').should('have.length', 100);
    });
  });
  