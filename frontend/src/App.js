import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import FAQList from './components/FAQList';
import AddFAQ from './components/AddFAQ';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<AddFAQ/>} />
        <Route path="/faq-list" element={<FAQList/>} />
        {/* Add other routes here */}
      </Routes>
    </Router>
  );
}

export default App;
