import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import FAQService from '../services/FAQService'; // Ensure this service handles API calls
import './AddFAQ.css';

function AddFAQ() {
  const [question, setQuestion] = useState('');
  const [answer, setAnswer] = useState('');
  const navigate = useNavigate();

  const handleSubmit = (event) => {
    event.preventDefault();
    const faqData = { question, answer };

    // Example API call using FAQService (adjust as per your service implementation)
    FAQService.addFAQ(faqData)
      .then(response => {
        console.log('FAQ added successfully:', response.data);
        // Handle success (e.g., show message, update UI)
      })
      .catch(error => {
        console.error('Error adding FAQ:', error);
        // Handle error (e.g., show error message)
      });
  };

  const handleShowFAQs = () => {
    navigate('/faq-list');
  };

  return (
    <div className="add-faq-container"> {/* Apply CSS class for container */}
      <h1> Patient Guide FAQs</h1>
      <h2>Add FAQ</h2>
      <form onSubmit={handleSubmit} className="add-faq-form">
        <label>
          Question:
          <input
            type="text"
            className="add-faq-input"
            value={question}
            onChange={(e) => setQuestion(e.target.value)}
            required
          />
        </label>
        <br />
        <br />
        <label>
          Answer:
          <input
            type="text"
            className="add-faq-input"
            value={answer}
            onChange={(e) => setAnswer(e.target.value)}
            required
          />
        </label>
        <br />
        <br />
        <button type="submit" className="add-faq-button">Add FAQ</button>
        <br />
        <br />
        <button type="button" className="show-faqs-button" onClick={handleShowFAQs}>Show FAQs</button>
      </form>
    </div>
  );
}

export default AddFAQ;
