import React, { useEffect, useState } from 'react';
import FAQService from '../services/FAQService';
import { useNavigate } from 'react-router-dom';
import './FAQList.css'; // Import CSS for styling

function FAQList() {
  const [faqs, setFAQs] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    fetchFAQs();
  }, []);

  const fetchFAQs = () => {
    FAQService.getAllFAQs()
      .then(response => {
        setFAQs(response.data);
      })
      .catch(error => {
        console.error('Error fetching FAQs:', error);
      });
  };

  const handleAddFAQs = () => {
    navigate('/');
  };

  return (
    <div className="faq-list-container"> {/* Apply CSS class for container */}
      <h2>FAQs</h2>
      <ul className="faq-list">
        {faqs.map(faq => (
          <li key={faq.id} className="faq-item">
            <strong>Question:</strong> {faq.question}
            <br />
            <strong>Answer:</strong> {faq.answer}
            <br />
          </li>
        ))}
      </ul>
      <button className="add-faq-button" onClick={handleAddFAQs}>Add FAQs</button>
    </div>
  );
}

export default FAQList;
