// src/services/FAQService.js
import axios from 'axios';

const API_URL = 'http://localhost:8081/api/faqs'; 

class FAQService {
  getAllFAQs() {
    return axios.get(API_URL);
  }

  addFAQ(faq) {
    return axios.post(API_URL, faq);
  }
}

export default new FAQService();
