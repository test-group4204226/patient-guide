package com.example.patientguide.Controller;
import com.example.patientguide.Model.FAQ;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

// Controller for FAQs
@RestController
@RequestMapping("/api/faqs")
@CrossOrigin(origins = "http://localhost:3000")
public class FAQController {

    private List<FAQ> faqs = new ArrayList<>();

    // Endpoint to get all FAQs
    @GetMapping
    public List<FAQ> getAllFAQs() {
        return faqs;
    }

    // Endpoint to add a new FAQ
    @PostMapping
    public FAQ addFAQ(@RequestBody FAQ faq) {
        faqs.add(faq);
        return faq;
    }
}