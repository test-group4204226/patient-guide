package com.example.patientguide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatientguideApplication {

	public static void main(String[] args) {
		SpringApplication.run(PatientguideApplication.class, args);
	}
}