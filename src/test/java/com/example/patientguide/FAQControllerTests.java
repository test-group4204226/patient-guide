package com.example.patientguide;
import com.example.patientguide.Controller.FAQController;
import com.example.patientguide.Model.FAQ;
import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class FAQControllerTests {

    // Unit test for adding FAQ
    @Test
    public void testAddFAQ() {
        FAQController controller = new FAQController();
        FAQ faq = new FAQ();
        faq.setQuestion("How to use the app?");
        faq.setAnswer("You can navigate through the menu for instructions.");
        FAQ addedFAQ = controller.addFAQ(faq);
        
        assertNotNull(addedFAQ);
        assertEquals(faq.getQuestion(), addedFAQ.getQuestion());
        assertEquals(faq.getAnswer(), addedFAQ.getAnswer());
    }

    // Unit test that might fail under certain conditions
    @Test
    public void testGetAllFAQs() {
        FAQController controller = new FAQController();
        
        // Simulate adding FAQs
        FAQ faq1 = new FAQ();
        faq1.setQuestion("How to login?");
        faq1.setAnswer("Use your registered email and password.");

        controller.addFAQ(faq1);
        
        // Retrieve FAQs
        List<FAQ> faqs = controller.getAllFAQs();
        
        // Assertion that might fail if faqs list is empty
        assertFalse(faqs.isEmpty());
    }

    // Test case designed to fail
    // @Test
    // public void testFAQNotInList() {
    //     FAQController controller = new FAQController();
 
    //     // Add an FAQ
    //     FAQ faq = new FAQ();
    //     faq.setQuestion("Will this test fail?");
    //     faq.setAnswer("Yes, it will.");

    //     controller.addFAQ(faq);

    //     // Retrieve FAQs
    //     List<FAQ> faqs = controller.getAllFAQs();

    //     // Assert that the FAQ is not in the list, which will fail
    //     boolean faqNotInList = faqs.stream().noneMatch(f -> f.getQuestion().equals("Will this test fail?") && f.getAnswer().equals("Yes, it will."));
    //     assertTrue(faqNotInList, "FAQ should not be in the list, but it is.");
    // }
}

